<?php

namespace Tests;

use Rudashi\PapersLibrary\App\Model\Paper;
use Rudashi\PapersLibrary\App\Model\PaperParameters;
use Totem\SamAcl\Testing\AttachRoleToUserTrait;
use Totem\SamAcl\Testing\CheckForbiddenEndpoints;
use Totem\SamAdmin\Testing\ApiCrudTest;

class PaperApiTest extends ApiCrudTest
{

    use AttachRoleToUserTrait,
        CheckForbiddenEndpoints;

    protected string $endpoint = 'papers';
    protected string $model = Paper::class;

    protected array $withoutFields = [
        'name'
    ];

    protected function createModel(array $attributes = []): Paper
    {
        /** @var Paper $model */
        $model = factory($this->model)->create($attributes);
        $model->parameters()->save(factory(PaperParameters::class)->make());
        $model->load('parameters');

        return $model;
    }

    public function test_store(): void
    {
        $model = $this->modelForStore();

        $this->post("/api/$this->endpoint/", $model)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'slug',
                    'name',
                    'description',
                    'parameters',
                    'public',
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'slug' => $model['slug'],
                'name' => $model['name'],
                'description' => $model['description'],
                'parameters' => [],
                'public' => $model['public'],
            ]);
    }

    public function test_failed_validation_store(): void
    {
        $data = array_merge(
            $this->arrayModel(true),
            ['parameters' => [ 0 => ['weight_name' => 'test']]]
        );
        $this->post("/api/$this->endpoint/", $data)
            ->assertStatus(422)
            ->assertJsonFragment([
                'error' => [
                    'code' => 422,
                    'message' => [
                        'name' => ['The name field is required.'],
                        'parameters.0.weight_value' => [__('The :attribute field is required when paper parameters are provided.', ['attribute' => 'parameters.0.weight_value'])],
                        'parameters.0.volume_name' => [__('The :attribute field is required when paper parameters are provided.', ['attribute' => 'parameters.0.volume_name'])],
                        'parameters.0.volume_value' => [__('The :attribute field is required when paper parameters are provided.', ['attribute' => 'parameters.0.volume_value'])],
                    ]
                ],
            ]);
    }

    public function test_get_all(): void
    {
        $model = $this->createModel();
        $parameter = $model->parameters->first();

        $this->withoutRole()->get("/api/$this->endpoint/all")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'slug',
                        'name',
                        'description',
                        'parameters' => [
                            '*' => [
                                'name',
                                'value',
                                'volumes',
                                'default',
                            ]
                        ],
                        'public',
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'id' => $model->id,
                'slug' => $model->slug,
                'name' => $model->name,
                'description' => $model->description,
                'parameters' => [
                    $parameter->weight_name => [
                        'name' => $parameter->weight_name,
                        'value' => $parameter->weight_value,
                        'volumes' => [
                            [
                                'name' => $parameter->volume_name,
                                'value' => $parameter->volume_value,
                                'default' => $parameter->default,
                            ]
                        ],
                        'default' => $parameter->default,
                    ]
                ],
                'public' => $model->public,
            ]);
    }

    public function test_get_all_only_public(): void
    {
        $model = $this->createModel(['public' => 1]);
        $parameter = $model->parameters->first();

        $this->withoutRole()->get("/api/$this->endpoint/")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'slug',
                        'name',
                        'description',
                        'parameters' => [
                            '*' => [
                                'name',
                                'value',
                                'volumes',
                                'default',
                            ]
                        ],
                        'public',
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'id' => $model->id,
                'slug' => $model->slug,
                'name' => $model->name,
                'description' => $model->description,
                'parameters' => [
                    $parameter->weight_name => [
                        'name' => $parameter->weight_name,
                        'value' => $parameter->weight_value,
                        'volumes' => [
                           [
                                'name' => $parameter->volume_name,
                                'value' => $parameter->volume_value,
                                'default' => $parameter->default,
                            ]
                        ],
                        'default' => $parameter->default,
                    ]
                ],
                'public' => 1,
            ]);
    }

    public function test_get_one(): void
    {
        $model = $this->createModel();
        $parameter = $model->parameters->first();

        $this->get("/api/$this->endpoint/$model->id")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'slug',
                    'name',
                    'description',
                    'parameters' => [
                        '*' => [
                            'name',
                            'value',
                            'volumes',
                            'default',
                        ]
                    ],
                    'public',
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'id' => $model->id,
                'slug' => $model->slug,
                'name' => $model->name,
                'description' => $model->description,
                'parameters' => [
                    $parameter->weight_name => [
                        'name' => $parameter->weight_name,
                        'value' => $parameter->weight_value,
                        'volumes' => [
                            [
                                'name' => $parameter->volume_name,
                                'value' => $parameter->volume_value,
                                'default' => $parameter->default,
                            ]
                        ],
                        'default' => $parameter->default,
                    ]
                ],
                'public' => $model->public,
            ]);
    }

    public function test_get_not_found(): void
    {
        $this->get("/api/$this->endpoint/$this->badId")
            ->assertNotFound()
            ->assertJsonFragment([
                'error' => [
                    'code' => 404,
                    'message' => __('Given id :code is invalid or paper not exist.', ['code' => $this->badId]),
                ],
            ]);
    }

    public function test_replace(): void
    {
        $model = $this->createModel();
        $new = $this->modelForStore();

        $this->put("/api/$this->endpoint/$model->id", $new)
            ->assertOk()
            ->assertJsonFragment([
                'id' => $model->id,
                'slug' => $model->slug,
                'name' => $new['name'],
                'description' => $new['description'],
                'public' => $new['public'],
            ]);
    }

    public function test_destroy(): void
    {
        $model = $this->createModel();

        $this->delete("/api/$this->endpoint/$model->id")
            ->assertOk()
            ->assertJsonFragment([
                'id' => $model->id,
                'slug' => $model->slug,
                'name' => $model->name,
                'description' => $model->description,
                'public' => $model->public,
            ]);
    }

    public function test_action_publish_paper(): void
    {
        $model = $this->createModel(['public' => 0]);

        $this->patch("/api/$this->endpoint/$model->id", ['action' => 'public'])
            ->assertOk()
            ->assertJsonFragment([
                'id' => $model->id,
                'slug' => $model->slug,
                'name' => $model->name,
                'description' => $model->description,
                'public' => 1,
            ]);
    }

    public function test_action_privatize_paper(): void
    {
        $model = $this->createModel(['public' => 1]);

        $this->patch("/api/$this->endpoint/$model->id", ['action' => 'private'])
            ->assertOk()
            ->assertJsonFragment([
                'id' => $model->id,
                'slug' => $model->slug,
                'name' => $model->name,
                'description' => $model->description,
                'public' => 0,
            ]);
    }

    public function test_wrong_action_to_patch(): void
    {
        $model = $this->createModel();

        $this->patch("/api/$this->endpoint/$model->id", ['action' => 'bla bla bla'])
            ->assertStatus(422)
            ->assertJsonFragment([
                'error' => [
                    'code' => 422,
                    'message' => 'Missing Parameter.'
                ],
            ]);
    }

    public function test_forbidden_endpoint_get_all(): void
    {
        self::assertTrue(true);
    }

    public function test_authorization_failed(): void
    {
        $model = $this->createModel();

        $this->withoutToken()->json('GET', "/api/$this->endpoint/$model->id")
            ->assertJson([
                'error' => [
                    'message' => 'The token could not be parsed from the request'
                ]
            ])
            ->assertStatus(400);
    }

}
