<?php

namespace Rudashi\PapersLibrary\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property \Rudashi\PapersLibrary\App\Model\Paper resource
 */
class PaperResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'id'            => $this->resource->id,
            'slug'          => $this->resource->slug,
            'name'          => __($this->resource->name),
            'description'   => $this->resource->description,
            'parameters'    => $this->resource->parameters,
            'public'        => $this->resource->public,
        ];
    }

}
