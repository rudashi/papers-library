<?php

namespace Rudashi\PapersLibrary\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class PaperCollection extends ApiCollection
{

    public $collects = PaperResource::class;

}
