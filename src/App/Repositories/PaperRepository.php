<?php

namespace Rudashi\PapersLibrary\App\Repositories;

use Rudashi\PapersLibrary\App\Model\Paper;
use Rudashi\PapersLibrary\App\Model\Weight;
use Rudashi\PapersLibrary\App\Model\PaperParameters;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamCore\App\Repositories\BaseRepository;
use Rudashi\PapersLibrary\App\Repositories\Contracts\PaperRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|Paper model
 */
class PaperRepository extends BaseRepository implements PaperRepositoryInterface
{

    public function model(): string
    {
        return Paper::class;
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']): Paper
    {
        if ($id === 0) {
            throw new RepositoryException( __('No paper id have been given.') );
        }
        /** @var Paper $data */
        $data = $this->model->with($relationships)->find($id, $columns);

        if ($data === null) {
            throw new RepositoryException( __('Given id :code is invalid or paper not exist.', ['code' => $id]), 404);
        }

        return $data;
    }

    public function findById(int $id = 0, array $columns = ['*']): Paper
    {
        return $this->findWithRelationsById($id, [], $columns);
    }

    public function all(array $columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc'): \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->with(['parameters'])->orderBy('name')->get();
    }

    public function allGrouped(array $columns = ['*']): \Illuminate\Support\Collection
    {
        return $this->all($columns)->transform(function(Paper $paper) {
            return $this->groupingParameters($paper);
        })->keyBy(static function(Paper $paper) {
            return $paper->slug;
        });
    }

    public function allPublic(array $columns = ['*']): \Illuminate\Support\Collection
    {
        return $this->allGrouped($columns)->where('public', 1);
    }

    public function groupingParameters(Paper $paper): Paper
    {
        $paper->setRelation(
            'parameters',
            $paper->parameters->groupBy('weight_name')->mapInto(Weight::class)
        );

        return $paper;
    }

    public function store(\Illuminate\Http\Request $request, int $id = 0): Paper
    {
        $paper = ($id === 0) ? $this->model : $this->find($id);

        if ($id === 0) {
            $paper->slug     = $request->input('slug');
        }

        $paper->name         = $request->input('name');
        $paper->description  = $request->input('description');
        $paper->public       = $request->input('public', 1);
        $paper->save();

        if ($id > 0) {
            $paper->parameters()->delete();
        }

        if ($request->filled('parameters')) {
            $paper->parameters()->saveMany(
                collect($request->input('parameters'))->mapInto(PaperParameters::class)
            );
        }

        return $paper;

    }

    public function publish(int $id, bool $toActivate = true): Paper
    {
        $model = $this->findById($id);

        $model->update( ['public' => (int) $toActivate] );

        return $model;
    }

    public function privatize(int $id): Paper
    {
        return $this->publish( $id, false);
    }

}
