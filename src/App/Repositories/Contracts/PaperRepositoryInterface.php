<?php

namespace Rudashi\PapersLibrary\App\Repositories\Contracts;

use Illuminate\Support\Collection;
use Rudashi\PapersLibrary\App\Model\Paper;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface PaperRepositoryInterface extends RepositoryInterface
{

    public function findById(int $id = 0, array $columns = ['*']): Paper;

    public function store(\Illuminate\Http\Request $request, int $id = 0): Paper;

    public function allGrouped(array $columns = ['*']): Collection;

    public function allPublic(array $columns = ['*']): Collection;

    public function groupingParameters(Paper $paper): Paper;

    public function publish(int $id): Paper;

    public function privatize(int $id): Paper;

}
