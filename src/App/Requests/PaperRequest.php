<?php

namespace Rudashi\PapersLibrary\App\Requests;

use Totem\SamCore\App\Requests\BaseRequest;

class PaperRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'slug'                          => 'required',
            'name'                          => 'required',
            'description'                   => 'nullable',
            'public'                        => 'integer|nullable',
            'parameters'                    => 'nullable',
            'parameters.*.weight_name'      => 'nullable|required_with:parameters.*.weight_value,parameters.*.volume_name,parameters.*.volume_value',
            'parameters.*.weight_value'     => 'nullable|required_with:parameters.*.weight_name,parameters.*.volume_name,parameters.*.volume_value',
            'parameters.*.volume_name'      => 'nullable|required_with:parameters.*.weight_name,parameters.*.weight_value,parameters.*.volume_value',
            'parameters.*.volume_value'     => 'nullable|required_with:parameters.*.weight_name,parameters.*.weight_value,parameters.*.volume_name',
        ];
    }

    public function messages(): array
    {
        return [
            'required_with' => __('The :attribute field is required when paper parameters are provided.'),
        ];
    }

}
