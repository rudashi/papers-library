<?php

namespace Rudashi\PapersLibrary\App\Requests;

class PaperPutRequest extends PaperRequest
{
    public function rules(): array
    {
        $base = parent::rules();
        unset($base['slug']);

        return $base;
    }

}
