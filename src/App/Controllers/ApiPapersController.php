<?php

namespace Rudashi\PapersLibrary\App\Controllers;

use Illuminate\Http\Request;
use Rudashi\PapersLibrary\App\Repositories\Contracts\PaperRepositoryInterface;
use Rudashi\PapersLibrary\App\Requests\PaperPutRequest;
use Rudashi\PapersLibrary\App\Requests\PaperRequest;
use Rudashi\PapersLibrary\App\Resources\PaperCollection;
use Rudashi\PapersLibrary\App\Resources\PaperResource;
use Totem\SamCore\App\Controllers\ApiController;

class ApiPapersController extends ApiController
{

    public function __construct(PaperRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index(): PaperCollection
    {
        return new PaperCollection($this->repository->allGrouped());
    }

    public function show(int $id): PaperResource
    {
        return new PaperResource(
            $this->getFromRequestQuery($this->repository->groupingParameters($this->repository->findById($id)))
        );
    }

    public function create(PaperRequest $request): PaperResource
    {
        return new PaperResource(
            $this->repository->groupingParameters($this->repository->store($request))
        );
    }

    public function replace(int $id, PaperPutRequest $request): PaperResource
    {
        return new PaperResource(
            $this->repository->groupingParameters($this->repository->store($request, $id))
        );
    }

    public function destroy(int $id): PaperResource
    {
        return new PaperResource($this->repository->delete($id));
    }

    public function update(int $id, Request $request): \Totem\SamCore\App\Resources\ApiResource
    {
        switch ($request->get('action')) {
            case 'public' :
                return new PaperResource($this->repository->publish($id));
            case 'private' :
                return new PaperResource($this->repository->privatize($id));
            default :
                return $this->response($this->error(422,'Missing Parameter.'));
        }
    }

    public function public(): PaperCollection
    {
        return new PaperCollection($this->repository->allPublic());
    }

}
