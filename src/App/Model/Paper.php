<?php

namespace Rudashi\PapersLibrary\App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int id
 * @property string slug
 * @property string name
 * @property string|null description
 * @property int public
 * @property \Illuminate\Database\Eloquent\Collection $parameters
 * @property \Illuminate\Support\Carbon|null deleted_at
 */
class Paper extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'created_at',
            'updated_at',
            'deleted_at',
            'pivot',
        ]);

        $this->fillable([
            'slug',
            'name',
            'description',
            'public',
        ]);

        parent::__construct($attributes);
    }

    public function parameters(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PaperParameters::class);
    }

}
