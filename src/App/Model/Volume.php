<?php

namespace Rudashi\PapersLibrary\App\Model;

class Volume
{

    public string $name;
    public string $value;
    public int $default;

    public function __construct(PaperParameters $parameters)
    {
        $this->name = $parameters->volume_name;
        $this->value = $parameters->volume_value;
        $this->default = $parameters->default;
    }

}
