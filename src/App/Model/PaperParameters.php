<?php

namespace Rudashi\PapersLibrary\App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string weight_name
 * @property string weight_value
 * @property string volume_name
 * @property string volume_value
 * @property int default
 */
class PaperParameters extends Model
{

    public const CREATED_AT = null;
    public const UPDATED_AT = null;

    public function __construct(array $attributes = [])
    {
        $this->setHidden([
            'paper_id',
        ]);

        $this->fillable([
            'weight_name',
            'weight_value',
            'volume_name',
            'volume_value',
            'default'
        ]);

        $this->setTable('papers_parameters');

        parent::__construct($attributes);
    }

    public function paper(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Paper::class);
    }

}
