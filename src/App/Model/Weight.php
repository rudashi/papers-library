<?php

namespace Rudashi\PapersLibrary\App\Model;

use Illuminate\Support\Collection;

class Weight
{

    public string $name;
    public string $value;
    public Collection $volumes;
    public int $default;

    public function __construct(Collection $collection)
    {
        $this->name = $collection->first()->weight_name;
        $this->value = $collection->first()->weight_value;
        $this->default = $collection->first()->default;
        $this->volumes = $collection->map(static function(PaperParameters $parameters) {
            return new Volume($parameters);
        });
    }

}
