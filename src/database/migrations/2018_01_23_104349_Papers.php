<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Papers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up(): void
    {
        try {
            Schema::create('papers', static function (Blueprint $table){
                $table->increments('id');
                $table->timestamps();
                $table->string('slug')->unique();
                $table->string('name');
                $table->text('description')->nullable();
                $table->tinyInteger('public')->default(0);
                $table->softDeletes();
            });

            Schema::create('papers_parameters', static function (Blueprint $table){
                $table->integer('paper_id')->unsigned();
                $table->string('weight_name');
                $table->string('weight_value');
                $table->string('volume_name');
                $table->string('volume_value');
                $table->tinyInteger('default')->default(0);

                $table->foreign('paper_id')->references('id')->on('papers')
                    ->onUpdate('cascade')->onDelete('cascade');

            });

        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => \Rudashi\PapersLibrary\Database\Seeds\PermissionSeeder::class
        ]);
        Artisan::call('db:seed', [
            '--class' => \Rudashi\PapersLibrary\Database\Seeds\PapersSeeder::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down(): void
    {
        Schema::drop('papers_parameters');
        Schema::drop('papers');

        (new \Rudashi\PapersLibrary\Database\Seeds\PermissionSeeder)->down();
    }

}
