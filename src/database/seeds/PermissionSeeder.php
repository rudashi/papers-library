<?php

namespace Rudashi\PapersLibrary\Database\Seeds;

use Illuminate\Database\Seeder;
use Totem\SamAcl\Database\PermissionTraitSeeder;

class PermissionSeeder extends Seeder
{
    use PermissionTraitSeeder;

    /**
     * Array of permissions.
     * %action  : view|create|edit|show|delete|modify
     * %name    : translatable from JSON
     *
     * @return array
     *  [
     *      'slug' => 'roles.modify',
     *      'name' => 'Can Modify Roles',
     *      'description' => 'Can modify roles',
     *  ]
     */
    public function permissions(): array
    {
        return [
            [
                'slug' => 'papers-library.view',
                'name' => 'View, create, edit and delete papers',
                'description' => 'Can CRUD on paper module',
            ],
        ];
    }

}
