<?php

use Faker\Generator as Faker;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */
$factory->define(\Rudashi\PapersLibrary\App\Model\PaperParameters::class, static function (Faker $faker) {
    $weight = $faker->randomElement([60, 70, 80, 90, 100, 120, 130, 150, 170, 200]);
    $volume = $faker->randomElement([0.7, 0.72, 0.82, 0.96, 1.00, 1.02, 1.22, 1.25, 2]);

    return [
        'weight_name' => $weight,
        'weight_value' => $weight,
        'volume_name' => $volume,
        'volume_value' => $volume,
        'default' => $faker->numberBetween(0, 1)
    ];
});
