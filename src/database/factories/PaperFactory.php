<?php

use Faker\Generator as Faker;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */
$factory->define(\Rudashi\PapersLibrary\App\Model\Paper::class, static function (Faker $faker) {
    return [
        'slug' => $faker->unique()->slug,
        'name' => $faker->unique()->domainWord,
        'description' => $faker->text,
        'public' => $faker->numberBetween(0, 1)
    ];
});
