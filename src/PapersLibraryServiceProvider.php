<?php

namespace Rudashi\PapersLibrary;

use Totem\SamCore\App\Traits\TraitServiceProvider;
use Illuminate\Support\ServiceProvider;

class PapersLibraryServiceProvider extends ServiceProvider
{
    use TraitServiceProvider;

    public function getNamespace(): string
    {
        return 'papers-library';
    }

    public function boot(): void
    {
        $this->loadAndPublish(
            __DIR__ . '/resources/lang',
            __DIR__ . '/database/migrations'
        );

        $this->loadRoutesFrom(__DIR__.'/routes/api.php');
    }

    public function register(): void
    {
        $this->registerEloquentFactoriesFrom(__DIR__. '/database/factories');

        $this->configureBinding([
            \Rudashi\PapersLibrary\App\Repositories\Contracts\PaperRepositoryInterface::class => \Rudashi\PapersLibrary\App\Repositories\PaperRepository::class
        ]);
    }

}
