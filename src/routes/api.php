<?php

use Illuminate\Support\Facades\Route;
use Rudashi\PapersLibrary\App\Controllers\ApiPapersController;

Route::group(['prefix' => 'api' ], static function() {

    Route::middleware(config('sam-admin.guard-api'))->group(static function() {

        Route::middleware('permission:papers-library.view')->prefix('papers')->group(static function() {
            Route::post('/', [ApiPapersController::class, 'create']);
            Route::get('{id}', [ApiPapersController::class, 'show']);
            Route::put('{id}', [ApiPapersController::class, 'replace']);
            Route::patch('{id}', [ApiPapersController::class, 'update']);
            Route::delete('{id}', [ApiPapersController::class, 'destroy']);

        });
    });

    Route::prefix('papers')->group(static function() {
        Route::get('all', [ApiPapersController::class, 'index']);
        Route::get('/', [ApiPapersController::class, 'public']);
    });

});
