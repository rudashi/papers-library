Sam papers library Component
================

This is a package manage papers, weights, and volumes which are used by SAM.

![Totem.com.pl](https://www.totem.com.pl/wp-content/uploads/2016/06/logo.png)

## General System Requirements

- [PHP >7.4.0](http://php.net/)
- [Laravel ~6.*](https://github.com/laravel/framework)
- [SAM-core ~1.*](https://bitbucket.org/rudashi/samcore)  
- [SAM-acl ~1.*](https://bitbucket.org/rudashi/samacl)  

## Quick Installation

If necessary, use the composer to download the library

```
$ composer require rudashi/papers-library
```

Remember to put repository in a composer.json

```
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/rudashi/papers-library.git"
    }
],
```

Do migration:

```
$ php artisan migrate
```

## Usage

### API

Check [openapi](openapi.json) file.

## Authors

* **Jan Rejnowski** - Junior programmer
* **Borys Żmuda** - Lead designer - [LinkedIn](https://www.linkedin.com/in/boryszmuda/), [Portfolio](https://rudashi.github.io/)
